from .app import db
from flask import url_for
from flask.ext.login import UserMixin
from .app import login_manager

class User(db.Model,UserMixin):
	username	=db.Column(db.String(50),primary_key=True)
	password	=db.Column(db.String(64))

	def get_id(self):
		return self.username

class Sondage(db.Model):
	id   = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(100))
	username = db.Column(db.String(50), db.ForeignKey("user.username"))
	user = db.relationship("User", backref=db.backref("sondages", lazy="dynamic", cascade="all, delete-orphan"))

	def __init__(self, name,username):
		self.name = name
		self.username=username

	def __repr__(self):
		return "<Sondage (%d) %s>" % (self.id, self.name)

	def serialize(self):
		return self.to_json()

	def to_json(self):
		return {
			"id" : self.id,
			"name" : self.name,
			"user" : self.user.username
		}

class Question(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	title = db.Column(db.String(120))
	sondage_id = db.Column(db.Integer, db.ForeignKey('sondage.id'))
	sondage = db.relationship("Sondage", backref=db.backref("questions", lazy="dynamic", cascade="all, delete-orphan"))

	def __repr__(self):
		return "<Question %s>" % (self.title)

	def serialize(self):
		return self.to_json()

	def to_json(self):
		return {
			"id" : self.id,
			"title" : str(self.title),
			"sondage_id" : self.sondage_id
		}

class SimpleQuestion(Question):
	firstAlternative = db.Column(db.String(120))
	secondAlternative = db.Column(db.String(120))
	firstChecked = db.Column(db.Boolean)

	def to_json(self):
		return {
		"firstAlternative" : self.firstAlternative,
		"secondAlternative" : self.secondAlternative,
		"firstChecked" : self.firstChecked
	}

	def serialize(self):
		return self.to_json()

	def __repr__(self):
		return "<SimpleQuestion %s %s %s>" % (self.title, self.firstAlternative, self.secondAlternative)

@login_manager.user_loader
def load_user(username):
	return User.query.get(username)

class ReponseSondage(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	user = db.Column(db.String(50))
	id_question = db.Column(db.Integer)
	reponse = db.Column(db.String(120))

	def to_json(self):
		return {
		"id" : self.id,
		"user" : self.user,
		"id_question" : self.id_question,
		"reponse" : self.reponse
		}

	def serialize(self):
		return self.to_json()