from .app import app, db
from flask import render_template, request, jsonify, abort,url_for,redirect
from .models import *
from flask.ext.wtf import Form,RecaptchaField
from wtforms import StringField, HiddenField,PasswordField,SelectField,TextAreaField,validators
from flask.ext.login import login_user,current_user,logout_user,login_required
from wtforms.validators import DataRequired
from sqlalchemy.exc import IntegrityError
from hashlib import sha256


#~ ============================================================Formulaires pour la connexion====================================================
class SigninForm(Form):
	username=StringField('Username',validators=[DataRequired()])
	password=PasswordField('Password',validators=[DataRequired()])
	next=HiddenField()

class LoginForm(Form):
	username=StringField('Username',validators=[DataRequired()])
	password=PasswordField('Password',validators=[DataRequired()])
	next=HiddenField()

	def get_authenticated_user(self):
		user=User.query.get(self.username.data)
		if user is None:
			return None
		m=sha256()
		m.update(self.password.data.encode())
		passwd=m.hexdigest()
		return user if passwd==user.password else None

@app.route("/")
def home():
	f=LoginForm()
	return render_template(
		"sondages.html",
		title="Questions",form=f)

#Recupérer les sondages
@app.route('/api/sondages', methods=['GET'])
@login_required
def get_sondages():
	#if not current_user.is_authenticated:
	#	return jsonify({"result" : False})
	#sondages = db.session.query(Sondage).filter(Sondage.username == current_user.username).all()
	sondages_ = db.session.query(Sondage).all()
	return jsonify(sondages=[e.serialize() for e in sondages_])

#Lecture d'un sondage pour un id
@app.route('/api/sondages/<int:id>', methods=['GET'])
@login_required
def get_sondage(id):
	sondage = db.session.query(Sondage).filter(Sondage.id == id).first()
	return jsonify(sondage.serialize())

#Récupération des questions d'un sondage donné
@app.route('/api/sondages/<int:id>/questions', methods=['GET'])
@login_required
def get_sondageQuestions(id):
	questions = db.session.query(Question).filter(Question.sondage_id == id).all()
	return jsonify(questions=[x.serialize() for x in questions])

#route de lecture de toutes les questions
@app.route('/api/questions', methods=['GET'])
@login_required
def get_questions():
	return jsonify(questions = [x.serialize() for x in db.session.query(SimpleQuestion).all()])

#lecture d'une question par son id
@app.route('/api/questions/<int:id>', methods=['GET'])
@login_required
def get_question(id):
	question = db.session.query(SimpleQuestion).filter(Question.id == id).first()
	return jsonify(question.to_json())

#route d'envoi d'une question au serveur
@app.route('/api/questions', methods=['POST'])
@login_required
def create_question():
	if not request.json or not 'title' in request.json:
		abort(400)
	title = request.json['title']
	sondage_id = request.json['sondage_id']
	print("on ajoute")
	firstAlternative = request.json["firstAlternative"]
	secondAlternative = request.json["secondAlternative"]
	firstChecked = request.json["firstChecked"]
	question = SimpleQuestion(title=title, sondage_id=sondage_id, firstAlternative=firstAlternative, secondAlternative=secondAlternative, firstChecked=firstChecked)
	print(question)
	db.session.add(question)
	db.session.commit()
	return jsonify(question.to_json()), 201
	#{"title":"oui","firstAlternative":"undefined","secondAlternative":"undefined","firstChecked":true,"sondage_id":"1"}

#~ route pour editer une question
@app.route('/api/questions/<int:id>', methods=['PUT'])
@login_required
def edit_question(id):
	question = SimpleQuestion()
	question.id=id
	question.sondage_id = request.json['sondage_id']
	question.title = request.json.get('title',"")
	question.firstAlternative = request.json.get('firstAlternative',"")
	question.secondAlternative = request.json.get('secondAlternative',"")
	print("id = " +str(id))
	print("title = " + request.json.get('title',""))
	print("firstAlternative = " + request.json.get('firstAlternative',""))
	print("secondAlternative = " + request.json.get('secondAlternative',""))
	db.session.query(SimpleQuestion).filter(SimpleQuestion.id == id).delete()
	db.session.add(question)
	db.session.commit()
	return jsonify(question.to_json())

#~route pour supprimer une question
@app.route('/api/questions/<int:id>', methods=['DELETE'])
@login_required
def delete_question(id):
	questions = SimpleQuestion.query.all()
	question = list(filter(lambda t : t.id == id, questions))
	if len(question)==0:
		abort(404)
	db.session.delete(question[0])
	db.session.commit()
	return jsonify({'result': True})

#~route pour créer un sondage
@app.route('/api/sondages', methods=['POST'])
@login_required
def create_sondage():
	if not request.json or not 'name' in request.json:
		abort(400)
	username=current_user.username
	sondage = Sondage(name=request.json['name'],username=username)
	db.session.add(sondage)
	db.session.commit()
	return jsonify(sondage.to_json()), 201


#~ route pour supprimer un sondage
@app.route('/api/sondages/<int:id>', methods=['DELETE'])
@login_required
def delete_sondage(id):
	sondage = db.session.query(Sondage).filter(Sondage.id == id).first()
	if not sondage:
		abort(400)
	db.session.delete(sondage)
	db.session.commit()
	return jsonify({'result': True})

#~ route pour l'auto-inscription
@app.route("/signin",methods=("GET","POST",))
def signin():
	f=SigninForm()
	if not f.is_submitted():
		f.next.data=request.args.get("next")
	elif f.validate_on_submit():
		try:
			m=sha256()
			m.update(f.password.data.encode())
			u=User(username=f.username.data,password=m.hexdigest())
			db.session.add(u)
			db.session.commit()
			next=f.next.data or url_for('home')
			login_user(u)
		except IntegrityError:
			return render_template("signin.html",form=f,LoginUtil=True)
		return redirect(next)
	return render_template("signin.html",form=f,LoginUtil=False)


#~ route pour la connexion
@app.route("/login",methods=("GET","POST",))
def login():
	f=LoginForm()
	if not f.is_submitted():
		f.next.data=request.args.get("next")
	elif f.validate_on_submit():
		user=f.get_authenticated_user()
		if user:
			login_user(user)
			next=url_for('home')
			return redirect(next)
	return render_template("sondages.html",form=f)


#~route pour la deconnexion
@app.route("/logout/")
def logout():
	logout_user()
	return redirect(url_for('home'))

@app.route("/repondre")
def repondre():
	return render_template("reponse.html")

@app.route("/api/reponse", methods=['POST'])
@login_required
def reponse():
	if not request.json or not 'id_question' in request.json or not 'reponse' in request.json:
		return str(request.json), 400
	reponse = ReponseSondage(user = current_user.username, id_question = request.json['id_question'], reponse = request.json['reponse'])
	db.session.add(reponse)
	db.session.commit()
	return jsonify({"request":True}), 201

@app.route("/api/reponse", methods=["GET"])
@login_required
def reponses_enregistrees():
	return jsonify(reponses = [x.serialize() for x in db.session.query(ReponseSondage).all()])