from .app import manager, db
from .models import Sondage, Question, SimpleQuestion

@manager.command
def loaddb(filename):
	'''
	Cree les tables et les remplit avec 
	les données du fichier json
	'''
	db.drop_all()
	db.create_all()

	import json
	with open(filename) as json_file:
		sondage = json.load(json_file)

	sond = Sondage(name=sondage['name'], username="toto")
	db.session.add(sond)
	db.session.commit()

	for question in sondage["questions"]:
		q = SimpleQuestion(title=question["title"], firstAlternative=question["firstAlternative"], secondAlternative=question["secondAlternative"], sondage_id=sond.id)
		db.session.add(q)
	db.session.commit()