Pour installer:
---------------
* Installer un venv : virtualenv -p python3 venv
* L'activer : source venv/bin/activate
* Installer les requirements : pip install -r requirements.txt
* Lancer le serveur : ./manage.py runserver
 
